library(gridExtra)
library(magrittr)
library(ggplot2)
library(dplyr)

# Loads a data set and names columns
load_die_throws <- function(path) {
    # Loading
    die_throws <- read.table(path)
    
    # Renaming
    die_throws <- die_throws %>%
        rename(
            index = V1,
            freq = V2
        )
}

# Transforms the table from absolute values into percentages
calc_throw_dist <- function(die_throws) {
    total_throws <- sum(die_throws$freq)
    
    data.frame(
        index = die_throws$index,
        p = die_throws$freq * (1.0 / total_throws)
    )
}

create_binom_index <- function(size, p) {
    data.frame(
        'index' = 0:size,
        'p' = dbinom(0:size, size, p)
    )
}

create_expected_throws <- function(size, p, total) {
    dist <- create_binom_index(size, p);
    data.frame(
        'index'=dist$index,
        'freq'=round(dist$p * total))
} 

flatten_die_throws <- function(die_throws) {
    flattened <- c()
    for (i in 1:nrow(die_throws))
        flattened <- append(flattened, 
                            rep(die_throws[i, 'index'], 
                                die_throws[i, 'freq']))
    
    return(flattened)
}

# Plots raw loaded data V1, V2
die_hist <- function(die_throws, name='Throw histogram') {
    g <- ggplot(die_throws, aes(x=index))

    g <- g + geom_bar(aes(y=freq), stat='identity')

    g <- g + ggtitle(name) + 
                xlab('Number of dies resulting in a value of 5 or 6') +
                ylab('Success frequency')

    g
}

# Plots distributino histograms
die_dist_hist <- function(die_throws, name="Throw distribution histogram") {
    die_dist <- calc_throw_dist(die_throws)
    plot(
        x = die_dist$index,
        y = die_dist$p,
        
        main = name,
        
        ylab = '%',
        xlab = 'Number of dies resulting in a value of 5 or 6 per experiment',
        
        t = 'h'
    )
}

plot_binomials <- function(observed, expected, title="Distribution comparison") {
    df <- data.frame(
        x <- observed$index,
        p1 <- observed$p,
        p2 <- expected$p)
    
    g <- ggplot(df, aes(x))
    
    g <- g + geom_bar(aes(y=p1, fill='green'), stat="identity", 
                color = 'green', alpha=0.2)
    g <- g + geom_bar(aes(y=p2, fill='red'), stat="identity", 
                color = 'red', alpha=0.2)
    
    g <- g + ggtitle(title) +
            xlab('Number of dies resulting in a value of 5 or 6 per experiment') +
            ylab('%')

    g <- g + scale_fill_manual(values=c('green', 'red'), 
                       name='Distribution coloring',
                       labels=c('observbed', 'expected'),)
    
    g
}

# Sum of logged binomial PMF
sl_dbinom <- function(p, size, data) {
    sum(dbinom(x = data, p = p, size = size, log = T))
}

# MLE for binomial
mle_binom <- function(dataset, sz, p_vec=seq(0, 1, 0.01)) {
    log_sums <- sapply(p_vec, sl_dbinom, sz, dataset)
    max_index = which.max(log_sums)
    
    p_vec[max_index]
}

collapse_categories <- function(data, lim=5) {
    while (TRUE) {
        min_index <- which.min(data$expected)
        if (data[min_index, ]$expected >= lim ||
            length(data$expected) == 1) {
            break;
        }
        
        r1 <- data[min_index, ]
        data <- data[-c(min_index), ]
        
        min_index <- which.min(data$expected)
        
        r2 <- data[min_index, ]
        data <- data[-c(min_index), ]
        
        data <- rbind(data, colSums(rbind(r1, r2)))
    }
    
    data
}

format_for_fit_test <- function(observed_throws, expected_throws) {
    df <- data.frame(
            'observed'=observed_throws$freq,
            'expected'=expected_throws$freq)
    collapse_categories(df)
}

estimate_p <- function(die_throws) {
    flatten <- flatten_die_throws((die_throws))
    p1 <- mle_binom(flatten, 12, seq(0, 1, 0.01))
    
    l = p1 - 0.01
    r = p1 + 0.01
    s = 0.0001
    
    mle_binom(flatten, 12, seq(l, r, s))
}

confidence_p <- function(die_throws, alpha=0.05) {
    p <- mean(flatten_die_throws(die_throws)) / 12
    q <- 1.0 - p
    n <- sum(die_throws$freq) * 12
    
    k <- sqrt((p * q) / n)
    
    l <- p + qnorm(alpha / 2) * k
    r <- p + qnorm(1 - (alpha / 2)) * k
    
    c(l, r)
}


# Assignments begin here
# Tables overview
# die_throws1
# die_throws2

# # Prepare constants
# SAMPLE_1_SIZE = 7006
# SAMPLE_2_SIZE = 26306

# die_throws1 <- load_die_throws('Data/die_f1.dat')
# die_throws2 <- load_die_throws('Data/die_f2.dat')
# # Dice distributions
# die_dist1 <- calc_throw_dist(die_throws1)
# die_dist2 <- calc_throw_dist(die_throws2)

# # Used in b)
# estimated_p1 <- estimate_p(die_throws1)
# estimated_p2 <- estimate_p(die_throws2)

# # B(12, estim_p1) and B(12, estim_p2)
# die_expected1 <- create_expected_throws(12, estimated_p1, 7006)
# die_expected2 <- create_expected_throws(12, estimated_p2, 26306)

# die_expected1
# die_expected2

# fit_df_1 <- format_for_fit_test(die_throws1, die_expected1)
# fit_df_2 <- format_for_fit_test(die_throws2, die_expected2)

# stat_fit_1 <- chisq.test(fit_df_1, rescale.p=TRUE)$statistic
# stat_fit_2 <- chisq.test(fit_df_2, rescale.p=TRUE)$statistic

# estimated_p1_fit <- pchisq(stat_fit_1, df=8, lower.tail=FALSE)
# estimated_p2_fit <- pchisq(stat_fit_2, df=9, lower.tail=FALSE)

# par(mfrow=c(2, 3))
# # Draw histograms here of the dices
# die_hist(die_throws1)
# die_hist(die_throws2)

# # Draw histograms here of the binomial B(12, 3) distribution, B(12, estimated_p1) and B(12, estimated_p2)
# dist_die_expected1 = calc_throw_dist(die_expected1)
# dist_die_expected2 = calc_throw_dist(die_expected2)

# plot_binomials(die_dist1, dist_die_expected1)
# plot_binomials(die_dist2, dist_die_expected2)

# # d)

# # e)
# TOTAL_1_THROWS = SAMPLE_1_SIZE * 12
# TOTAL_2_THROWS = SAMPLE_2_SIZE * 12

# SUCCESSES_1_THROWS = die_throws1$freq * 0:12
# SUCCESSES_2_THROWS = die_throws2$freq * 0:12

# res1 = binom.test(sum(SUCCESSES_1_THROWS), TOTAL_1_THROWS, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)
# res2 = binom.test(sum(SUCCESSES_2_THROWS), TOTAL_2_THROWS, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)

# print("95% Confidence interval for sample I - using the estimated p: ")
# res1$conf.int

# print("95% Confidence interval for sample II - using the estimated p: ")
# res2$conf.int

# # Display results
# res1
# res2

# #################################################               USELESS                 #################################################################
# #e1)
# res1 = binom.test(sum(die_throws1$freq), SAMPLE_1_SIZE, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)
# res2 = binom.test(sum(die_throws2$freq), SAMPLE_2_SIZE, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)

# print("95% Confidence interval for sample I - using the estimated p: ")
# res1$conf.int

# print("95% Confidence interval for sample II - using the estimated p: ")
# res2$conf.int

# #e2)
# die_third1 <- create_expected_throws(12, 1/3, 7006)
# die_third2 <- create_expected_throws(12, 1/3, 26306)
# dist_third1 = calc_throw_dist(die_third1)
# dist_third2 = calc_throw_dist(die_third2)

# # Construct table for Chi-Square
# fit_e1 <- format_for_fit_test(die_throws1, die_third1)
# fit_e2 <- format_for_fit_test(die_throws2, die_third2)

# # Compare, grab the p-value
# is_fit_e1 <- chisq.test(fit_e1, rescale.p=TRUE)
# is_fit_e2 <- chisq.test(fit_e2, rescale.p=TRUE)

# is_fit_e1
# is_fit_e2

# binom.test(sum(die_throws1$freq), SAMPLE_1_SIZE * 12, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)
# binom.test(sum(die_throws2$freq), SAMPLE_2_SIZE * 12, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)

# binom.test(sum(die_throws1$freq), SAMPLE_1_SIZE * 3, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)
# binom.test(sum(die_throws2$freq), SAMPLE_2_SIZE * 3, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)

# binom.test(sum(die_third1$freq), 7006, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)

# # d)
# # 95% confidence interval for p = 1 / 3
# res1 = binom.test(die_throws1$freq, SAMPLE_1_SIZE, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)
# res2 = binom.test(die_throws2$freq, SAMPLE_2_SIZE, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)

# p_val_1 = res1$p.value
# success_estimate_1 = res1$estimate
# success_null_1 = res1$null.value

# p_val_2 = res2$p.value
# success_estimate_2 = res2$estimate
# success_null_2 = res2$null.value

# print("P value of binomial test for sample I: ")
# p_val_1
# print("Success estimate of binomial test for sample I: ")
# success_estimate_1
# print("The prob of success under the null p(1/3) in sample I: ")
# success_null_1

# print("P value of binomial test for sample II: ")
# p_val_2
# print("Success estimate of binomial test for sample II: ")
# success_estimate_2
# print("The prob of success under the null p(1/3) in sample II: ")
# success_null_2

# # Let's draw a histogram of the binomial distribution B(12, 1/3)
# x <- 0:12
# y <- dbinom(x, 13, 1/3)
# hist(y, breaks=13, main='B(13, 1/3)',
#      xlab='x-Axis', ylab='y-Axis/Freqs')
# #############################################               USELESS                 #################################################################
