---
title: "Report - Koala"
author:
- Brekalo, Tvrtko
- Čeple, Kristijan
- Zec, Mario
date: "29/5/2020"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
source("./A/src.r")
```


# Task A

### Data loading and formatting  

We defined a function `load_die_throws` in `A/src.r` which takes 
an input path to a `.dat` file and returns a data frame encapsulating
success rate for each possible outcome of a 12 die throw experiment.

*  `index` number of dies resulting in a value of 5 or 6
*  `freq` outcome success frequency 

```{r}
die_throws1 <- load_die_throws('Data/die_f1.dat')
die_throws2 <- load_die_throws('Data/die_f2.dat')
```

Format examle:
```{r}
die_throws1
```

Eg. Column `index` with a row value *y* aligned with a column `freq` row with value *x*
means that *y* dies with value 5 or 6 in a 12 die throw experiment occured in *x* iterations. <br/><br/>

Since sample sizes are predefined, we use constants to refer to them in future subtasks.

```{r}
SAMPLE_1_SIZE = 7006
SAMPLE_2_SIZE = 26306
```

### Subtask a) histogram plots

For plotting throw frequencies we used a `ggplot2` library and `gridExtra`
for markdown formatting.

```{r}

h1 <- die_hist(die_throws1, name='Sample I')
h2 <- die_hist(die_throws2, name='Sample II')

grid.arrange(h1, h2, ncol=1, nrow=2)
```

### Subtask b) <br/> Goodness-of-Fit test for prescribed and estimated bionmial distributions

*$H_0$*: Sample fits the desired distribution <br/>
*$H_1$*: Sample does not fit the desired distribution <br/>
**$\alpha$ ** = $0.05$ falls to the right side of $\chi^2$ the distribution. <br/><br/>
In other words, if the calculated *p* value exceeds $0.95$, we reject *$H_0$*
in favour of *$H_1$*.
<br/><br/>

First subset test for a predifined distribution
*B*(12, 1/3):

```{r}
die_expected1 <- create_expected_throws(12, 1/3, SAMPLE_1_SIZE)
fit_df_1 <- format_for_fit_test(die_throws1, die_expected1)
```
Generate data paired and formated alongsdie provided sample I:
```{r}
fit_df_1
```
Fit test:
```{r}
chisq.test(fit_df_1, rescale.p=TRUE)
```

Given value does not reject *$H_0$*, sample I fits the model.

```{r}
die_expected2 <- create_expected_throws(12, 1/3, SAMPLE_2_SIZE)
fit_df_2 <- format_for_fit_test(die_throws2, die_expected2)
```
Generate data paired and formated alongsdie provided sample II:
```{r}
fit_df_2
```
Fit test:
```{r}
chisq.test(fit_df_2, rescale.p=TRUE)
```

Given value does not reject *$H_0$* but we can conclude that the sample II fits the predefined distribution model much better than the first sample with a noticeably higher $\chi^2$ statistic. <br/><br/>

#### Goodness-of-Fit test for the estimated *p*

For *p* estimation we used a self-implemented MLE algorithm found in
`A/src.r`. A standard `chisq.test` result is further re-evaluated
using a `pchisq` function with a reduced *degrees-of-freedom* parameter to 
compensate for the *p* estimation

Estimated *p* value for the first data set:
```{r}
estimated_p1 <- estimate_p(die_throws1)
estimated_p1
```


```{r}
die_estimated1 <- create_expected_throws(12, estimated_p1, SAMPLE_1_SIZE)
fit_df_1 <- format_for_fit_test(die_throws1, die_estimated1)

stat_fit_1 <- chisq.test(fit_df_1, rescale.p=TRUE)$statistic
pchisq(stat_fit_1, df=8, lower.tail=FALSE)
```

<br/><br/>

```{r}
estimated_p2 <- estimate_p(die_throws2)
estimated_p2
```

```{r}
die_estimated2 <- create_expected_throws(12, estimated_p2, SAMPLE_2_SIZE)
fit_df_2 <- format_for_fit_test(die_throws2, die_estimated2)

stat_fit_2 <- chisq.test(fit_df_2, die_estimated2)$statistic
pchisq(stat_fit_2, df=9, lower.tail=FALSE)
```

We can observe that the *p-values* for a Goodness-of-Fit test for estimated probabilities are still not high enough to reject the **$H_0$** hypothesis with $\alpha = 0.05$ but the estimated model for first sample provides a much better fit than the prescribed one while for the second sample it
is contrary.

### Subtask c) Graphical comparison 

Plotting is done via custom `plot_binomials` function which is a wrapper
for `ggplot2 geom_bar` with manual legend specification.

#### Observed vs. defined distribution
```{r}
die_dist_defined <- create_binom_index(12, 1/3)

die_dist1 <- calc_throw_dist(die_throws1)
die_dist2 <- calc_throw_dist(die_throws2)

h1 <- plot_binomials(die_dist1, die_dist_defined, 'Sample I')
h2 <- plot_binomials(die_dist2, die_dist_defined, 'Sample II')

grid.arrange(h1, h2, ncol=1, nrow=2)
```


#### Observed vs. estimated distribution

```{r}

die_dist_estimated1 <- calc_throw_dist(die_estimated1)
die_dist_estimated2 <- calc_throw_dist(die_estimated2)

h1 <- plot_binomials(die_dist1, die_dist_estimated1, 'Sample I')
h2 <- plot_binomials(die_dist2, die_dist_estimated2, 'Sample II')

grid.arrange(h1, h2, ncol=1, nrow=2)
```

### Subtask d) *`p`* confidence interval estimation

Confidence interval is calculated via `confidence_p` funciton implemented
in `A/src.r`. The same can be obtained from a standard `r` function
`binom.test` as it's done in subtask *e)*.

```{r}
confidence_p(die_throws1, alpha=0.05)
```
```{r}
confidence_p(die_throws2, alpha=0.05)
```


### Subtask e) Testing **$H_0$**
We need to test whether $p = \frac{1}{3}$. In the assignment above, we have manually implemented the confidence interval calculation and got a confidence interval. In this assignment using R's built-in `binom.test(...)` function, we shall confirm that those 2 intervals match.

```{r}
TOTAL_1_THROWS = SAMPLE_1_SIZE * 12
TOTAL_2_THROWS = SAMPLE_2_SIZE * 12

SUCCESSES_1_THROWS = die_throws1$freq * 0:12
SUCCESSES_2_THROWS = die_throws2$freq * 0:12

res1 <- binom.test(sum(SUCCESSES_1_THROWS), TOTAL_1_THROWS, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)
res2 <- binom.test(sum(SUCCESSES_2_THROWS), TOTAL_2_THROWS, p = 1/3, alternative = c("two.sided"), conf.level = 0.95)
```
95% Confidence interval for sample I - using the $p = \frac{1}{3}$:
```{r}
res1$conf.int
```
95% Confidence interval for sample II - using the $p = \frac{1}{3}$:
```{r}
res2$conf.int

# Display results
res1
res2

```

$\frac{1}{3}$ falls out of the confidence interval for both samples.<br/>
$H_0: p = \frac{1}{3}$ is rejected in favour of $H_1: p \neq \frac{1}{3}$

# Task B
### Data loading and formatting
Merely load the data using R's built-in function `read.table(...)`, and then verify that data using `head(data)`
and `names(data)` functions.
```{r}
forbes = read.table("Data/forbes.dat", header = FALSE)
names(forbes) = c("boilpt", "atmPres")
head(forbes)
hooker = read.table("Data/hooker.dat", header = FALSE)
names(hooker) = c("boilpt", "atmPres")
head(hooker)
```

### Subtask a) Data plotting
We need to plot data. First we plot the `forbes` and the `hooker` data sets. Then we combine them together into a single dataset using the `rbind(...)` function, and plot this final dataset. It will be used in the coming subtasks.
```{r}
par(mfrow=c(1, 3))
plot(forbes)
plot(hooker)
# Time to pair them
atmboil_data = rbind(forbes, hooker)
head(atmboil_data)
plot(atmboil_data)
```
According to the plots, I'd say that most likely there is a linear relationship between *x* and *y* axes.

### Subtask b) Linear regression -> linear and quadratic fit models
Using `R`'s built-in function `lm` we produce the two fits and then plot them.
```{r}
par(mfrow=c(1,2))
atmboil_lin_fit = lm(atmPres~boilpt,data=atmboil_data)
atmboil_quadr_fit = lm(atmPres~boilpt + I(boilpt^2),data=atmboil_data)
```
#### Linear fit summary:
$y = \theta_0 + \theta_1x$
```{r}
summary(atmboil_lin_fit)
```
```{r}
plot(atmboil_data)
lines(atmboil_data$boilpt, atmboil_lin_fit$fitted.values,col='red')
```
#### Quadratic fit summary
$y = \theta_0 + \theta_1x + \theta_2x^2$
```{r}
summary(atmboil_quadr_fit)
```
```{r}
plot(atmboil_data)
#lines(atmboil_data$boilpt, atmboil_quadr_fit$fitted.values, col='red')
f = function(x, coeffs)
  return(coeffs[[1]] + coeffs[[2]] * x + coeffs[[3]] * x^2)
curve(f(x, atmboil_quadr_fit$coefficients), add = TRUE, col = "red")
```

The plotting function `f` was adapted from the subject exercises, and used in tandem with the `curve(...)` function, as opposed
to the usual `plot(...)` function, which in this case was used to draw the linear fit.

## Subtask c) Draw the residuals graph, standardised residuals graph, check whether the standardised residuals come from the *Gauss distribution*
Just as the title of the subtask says. Checking whether the standardised residuals come from the *Gauss distribution* can be done in 2 ways:

*  *Normal probability plots* and *QQPlots*
*  *Kolmogorov - Smirnov test*

For fancy QQPlots we've used the `qqPlot(...)` function from the `car` library. For the *Normal probability plots* we've used the built-in R function `qqnorm(...)`, and the `qqline(...)` functions. The latter draws the line on the plot, to which the residuals should be as close as possible. Therefore, the line serves as a visual indicator.
```{r}
par(mfrow = c(1,2))
atmboil_lin_res = resid(atmboil_lin_fit)
plot(
  atmboil_data$boilpt, atmboil_lin_res,
  ylab = "Residuals", xlab = "Boiling point",
  main = "(Non-std) Residuals graph for linear fit",
  #ylim = c(-1, 1)
)
atmboil_quadr_res = resid(atmboil_quadr_fit)
plot(
  atmboil_data$boilpt, atmboil_quadr_res,
  ylab = "Residuals", xlab = "Boiling point",
  main = "(Non-std) Residuals graph for quadratic fit",
  #ylim = c(-1, 1)
)
# Time for standardised residuals!
atmboil_lin_res_std = rstandard(atmboil_lin_fit)
plot(
  atmboil_data$boilpt, atmboil_lin_res_std,
  ylab = "Residuals", xlab = "Boiling point",
  main = "Standardised Residuals graph for linear fit",
  #ylim = c(-1, 1)
)
atmboil_quadr_res_std = rstandard(atmboil_quadr_fit)
plot(
  atmboil_data$boilpt, atmboil_quadr_res_std,
  ylab = "Residuals", xlab = "Boiling point",
  main = "Standardised Residuals graph for quadratic fit",
  #ylim = c(-1, 1)
)
# Time to test the normality of these residuals!
#par(mfrow = c(1,1))
qqnorm(
  atmboil_lin_res_std,
  ylab = "Standardised residuals",
  xlab = "Normal scores",
  main = "QQ Normal Probability Plot of Linear Fit"
)
qqline(atmboil_lin_res_std, col='steelblue', lwd = 2)
qqnorm(
  atmboil_quadr_res_std,
  ylab = "Standardised residuals",
  xlab = "Normal scores",
  main = "QQ Normal Probability Plot of Quadratic Fit"
)
qqline(atmboil_quadr_res_std, col='steelblue', lwd = 2)
print("Time for some fancy qqplots!")
library("car")
qqPlot(atmboil_lin_res_std)
qqPlot(atmboil_quadr_res_std)
print("Upon closer graph inspection, linear fit qqnorm doesn't seem to be normal. The upper half is kinda off!")
print("Quadratic one seem way better tho!")
print("I've made those assumptions before running any Kolmogorov-Smirnov tests. Time to give them a run")
ks.test(atmboil_lin_res_std,'pnorm')
ks.test(atmboil_quadr_res_std,'pnorm')
print("I was wrong. Well, partly(half). All the p values are pretty big, in some cases massive. We can never reject H0")
```

Before running the *Kolmogorov-Smirnov tests* I wouldn't have said that the linear fit came from the *Gauss distribution*, but the tests have confirmed that both linear and quadratic fit standardised residuals indeed ***DO*** come from the *Gauss distribution*.<br/>
*$H_0$*: Standardised residuals come from the *Gauss distribution*<br/>
*$H_1$*: Standardised residuals ***DO NOT*** come from the *Gauss distribution*

#### Subtask d) Calculate the $R^2$ statistic --> are the linear fit and quadratic fit models satisfactory? Which should we rather choose?
This is actually included in the 2.b) subtask. There, it is visible that $R^2$(and actually ***Adjusted $R^2$*** is what we should be looking at) is better in *quadratic models*. Let us print those values out and take a closer look at them!

$R^2$ shows us how good our linear regression fits are. However, it does only that, and doesn't account for the number of terms used in the fit(which can then contribute to overfitting), and neither does it account for the fact that some terms are more useful, and some are less useful.

***Adjusted $R^2$*** to the rescue! It provides information on how good the fit is, while also taking into account the number and usefulness/contribution of terms. Adding terms to the fit which contribute will increase the ***Adjusted $R^2$***, while adding non-contributing/useless terms will do the opposite. Therefore, it follows that the following will always hold: $Adj.R^2 \le R^2$!
```{r}
lin_adjr2 <- summary(atmboil_lin_fit)$adj.r.squared
quadr_adjr2 <- summary(atmboil_quadr_fit)$adj.r.squared
lin_r2 <- summary(atmboil_lin_fit)$r.squared
quadr_r2 <- summary(atmboil_quadr_fit)$r.squared
print(paste("Linear fit R^2 = ", lin_r2))
print(paste("Quadratic fit R^2 = ", quadr_r2))
print(paste("Linear fit adj R^2 = ", lin_adjr2))
print(paste("Quadratic fit adj R^2 = ", quadr_adjr2))
```

Both models estimate very well, however maybe the *quadratic fit* might be an *overfit*. Because we want to keep a certain ***generalisation*** ability, going with the *linear fit* would probably serve us better.

## Subtask e)
F test:
  - H0: linear model is enough
  - H1: quadratic model should be used.
pvalue ~ 10e-17
====> reject H0
```{r}

boilpt_as_x = atmboil_lin_fit$coefficients['boilpt']
intercept_with_y = atmboil_lin_fit$coefficients['(Intercept)']

sse_lin_reducible = anova(atmboil_lin_fit)["Residuals", "Sum Sq"]
sse_quadr_full = anova(atmboil_quadr_fit)["Residuals", "Sum Sq"]

n = nrow(forbes) + nrow(hooker)
f = (sse_lin_reducible - sse_quadr_full)/(sse_quadr_full/(n - 2 - 1))
pv = pf(f, df1 = 1, df2 = n - 2 - 1, lower.tail = FALSE)
print(pv)

# intervali pouzdanosti za kvadratni model
confint(atmboil_quadr_fit, level = 0.95)

```
## Subtask f)
Red curves present upper and lower bound of confidence interval for the mean response \(\mu_{Y|x0}\).
Blue curves present upper and lower bound of prediction interval for a single response \(y_0\).

```{r}

min_boilpt = min(atmboil_data$boilpt)
max_boilpt = max(atmboil_data$boilpt)

f2 = function(x){
  return(predict(atmboil_quadr_fit, interval = "confidence", newdata = data.frame(boilpt = x))[2])
}

f3 = function(x){
  return(predict(atmboil_quadr_fit, interval = "confidence", newdata = data.frame(boilpt = x))[3])
}

f4 = function(x){
  return(predict(atmboil_quadr_fit, interval = "prediction", newdata = data.frame(boilpt = x))[2])
}

f5 = function(x){
  return(predict(atmboil_quadr_fit, interval = "prediction", newdata = data.frame(boilpt = x))[3])
  
}

plot(
  atmboil_data$boilpt, 
  atmboil_data$atmPres,
  ylab = "Confidence (red) and prediction (blue) intervals",
  xlab = "Boiling point",
)
q = Vectorize(f2)
curve(q, col = "red", add = TRUE, from = min_boilpt, to = max_boilpt, new = T)
w = Vectorize(f3)
curve(w, col = "red", add = TRUE, from = min_boilpt, to = max_boilpt, new = T)
h = Vectorize(f4)
curve(h, col = "blue", add = TRUE, from = min_boilpt, to = max_boilpt, new = T)
g = Vectorize(f5)
curve(g, col = "blue", add = TRUE, from = min_boilpt, to = max_boilpt, new = T)


```
