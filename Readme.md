Statistical Data Analysis project
Theme: Model fitting and Linear regression

Members:
	Tvrtko Brekalo
	Kristijan Ceple
	Mario Zec

Technologies used: R, RStudio

Originally held by Tvrtko at github, I've moved it here.